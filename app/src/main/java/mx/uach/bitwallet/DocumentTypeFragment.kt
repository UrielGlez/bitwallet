package mx.uach.bitwallet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import com.google.android.material.floatingactionbutton.FloatingActionButton

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DocumentTypeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DocumentTypeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_document_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //(activity as AppCompatActivity?)!!.supportActionBar!!.title = "Crear Documento"
        //(activity as MainActivity).backButtonOnToolbar(true)


        val btnDebitCreditCard = view.findViewById<LinearLayout>(R.id.ly_vw_CDCard)
        val btnId = view.findViewById<LinearLayout>(R.id.ly_vw_Identification)
        val btnDriverLicence = view.findViewById<LinearLayout>(R.id.ly_vw_DrvLicense)
        val btnCardPlate = view.findViewById<LinearLayout>(R.id.ly_vw_Plate)
        val btnGiftCard = view.findViewById<LinearLayout>(R.id.ly_vw_GiftCard)

        btnDebitCreditCard.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.addingDCCardFragment)
        })

        btnId.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.addingIDFragment)
        })

        btnDriverLicence.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.addingDLFragment)
        })

        btnCardPlate.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.addingCarPlateFragment)
        })

        btnGiftCard.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.addingGCFragment)
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DocumentTypeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DocumentTypeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}