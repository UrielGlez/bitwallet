package mx.uach.bitwallet.models

class User (
        var firstName: String,
        var lastName: String,
        var email: String,
        var pin: String
) {
}