package mx.uach.bitwallet.models

import java.util.*

class Card(
    var _notes: String,
    var _type_card: String,
    var _user_id: String,
    var _card_number: String,
    var _serie: String,
    var _account_name: String,
    var _car_model: String,
    var _title: String,
    var _curp: String,
    var _state: String,
    var _municipality: String,
    var _section: String,
    var _validity_date: Date,
    var _id: String? = null
): Comparable<Card> {
    override fun compareTo(other: Card): Int {
        return this._title.first().toLowerCase().compareTo(other._title.first().toLowerCase())
    }
}

enum class TypeCard() {
    CREDIT_DEBIT_CARD,
    DRIVER_LICENSE,
    LICENSE_PLATE,
    GIFT_CARD,
    ID_CARD
}