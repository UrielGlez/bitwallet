package mx.uach.bitwallet

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import mx.uach.bitwallet.models.Card

class SharedViewModel : ViewModel() {
    val selected = MutableLiveData<Card>()
    val createMode = MutableLiveData<Boolean>()

    fun select(item: Card) {
        selected.value = item
    }

    fun setCreateMode(op: Boolean) {
        createMode.value = op
    }
}