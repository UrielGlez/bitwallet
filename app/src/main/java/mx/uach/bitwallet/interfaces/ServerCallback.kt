package mx.uach.bitwallet.interfaces

import org.json.JSONObject

interface ServerCallback {
    fun onSuccess(result: JSONObject?, statusCode: Int)
}

interface ServerStringCallback {
    fun onSuccess(result: String, statusCode: Int)
}