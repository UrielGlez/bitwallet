package mx.uach.bitwallet

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.material.navigation.NavigationView
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import android.app.Activity
import mx.uach.bitwallet.models.TypeCard


class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var drawer: DrawerLayout
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var navController: NavController
    private lateinit var preferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private val toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        preferences = getSharedPreferences("sesiones", Context.MODE_PRIVATE)!!
        editor = preferences.edit()
        val user: HashMap<*, *> = Gson().fromJson(preferences.getString("user", null), HashMap::class.java)

        // assigning ID of the toolbar to a variable
        val toolbar: Toolbar = findViewById(R.id.toolbar_main)

        toolbar.bringToFront()

        // using toolbar as ActionBar
        setSupportActionBar(toolbar)

        drawer = findViewById(R.id.drawer_layout)

        toggle = ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val navigationView: NavigationView = findViewById(R.id.navigation_view)
        val headerView = navigationView.getHeaderView(0)
        val navUserName = headerView.findViewById<TextView>(R.id.user_name)
        val navUserEmail = headerView.findViewById<TextView>(R.id.user_email)
        navUserName.text = "${user["_first_name"]} ${user["_last_name"]}"
        navUserEmail.text = "${user["_email"]}"

        val navigationFooterView: NavigationView = findViewById(R.id.navigation_view_bottom)

        navigationView.setNavigationItemSelectedListener(this)
        navigationFooterView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_close_sesion -> {
                editor.putBoolean("sesion", false)
                editor.apply()
                this.finish()
                this.startActivity(Intent(applicationContext, MainActivity::class.java))
            }
            R.id.nav_all_items -> {
                startHomeActvity(null)
            }
            R.id.nav_cards -> {
                startHomeActvity(TypeCard.CREDIT_DEBIT_CARD.toString())
            }
            R.id.nav_gift_cards -> {
                startHomeActvity(TypeCard.GIFT_CARD.toString())
            }
            R.id.nav_ids -> {
                startHomeActvity(TypeCard.ID_CARD.toString())
            }
            R.id.nav_licenses -> {
                startHomeActvity(TypeCard.DRIVER_LICENSE.toString())
            }
            R.id.nav_plates -> {
                startHomeActvity(TypeCard.LICENSE_PLATE.toString())
            }
        }

        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun startHomeActvity(typeCard: String?) {
        val myIntent = Intent(applicationContext, HomeActivity::class.java)
        myIntent.putExtra("type_card", typeCard)
        this.finish()
        this.startActivity(myIntent)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.appbar_actions, menu);
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //navController = Navigation.findNavController(this, R.id.fragment)
        when (item.itemId) {
            R.id.action_favorite -> {
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                true
            }

            else -> {
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                super.onOptionsItemSelected(item)
            }
        }

        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun setActionBarTitle(title: String): Unit {
        val v: View? = supportActionBar?.customView
        val titleTxtView: TextView? = v?.findViewById(R.id.toolbar_title)
        titleTxtView?.text = title
    }
}
