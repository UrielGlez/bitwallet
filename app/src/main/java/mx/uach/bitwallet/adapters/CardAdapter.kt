package mx.uach.bitwallet.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.uach.bitwallet.R
import mx.uach.bitwallet.models.Card

class CardAdapter(val cards: List<Card>) : RecyclerView.Adapter<CardAdapter.ViewHolder>() {
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
//        val tvCardNumberValue = listItemView.findViewById<TextView>(R.id.tvCardNumber)
//        val tvSerieValue = listItemView.findViewById<TextView>(R.id.tvSerie)
//        val tvAccountNameValue = listItemView.findViewById<TextView>(R.id.tvAccountName)
//        val tvCarModelValue = listItemView.findViewById<TextView>(R.id.tvCarModel)
        val tvTitleValue = listItemView.findViewById<TextView>(R.id.tvTitle)
        val tvPillTextValue = listItemView.findViewById<TextView>(R.id.pillText)
//        val tvCurpValue = listItemView.findViewById<TextView>(R.id.tvCurp)
//        val tvStateValue = listItemView.findViewById<TextView>(R.id.tvState)
//        val tvMunicipalityValue = listItemView.findViewById<TextView>(R.id.tvMunicipality)
//        val tvSectionValue = listItemView.findViewById<TextView>(R.id.tvSection)
//        val tvValidityDateValue = listItemView.findViewById<TextView>(R.id.tvValidityDate)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val gradeView = inflater.inflate(R.layout.card_item, parent, false)
        return ViewHolder(gradeView)
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.tvCardNumberValue.text = cards.elementAt(position).cardNumber
//        holder.tvSerieValue.text = cards.elementAt(position).serie
//        holder.tvAccountNameValue.text = cards.elementAt(position).accountName
//        holder.tvCarModelValue.text = cards.elementAt(position).carModel
        holder.tvTitleValue.text = cards.elementAt(position)._title
        holder.tvPillTextValue.text = cards.elementAt(position)._title.capitalize().substring(0, 1)
//        holder.tvCurpValue.text = cards.elementAt(position).curp
//        holder.tvStateValue.text = cards.elementAt(position).state
//        holder.tvMunicipalityValue.text = cards.elementAt(position).municipality
//        holder.tvSectionValue.text = cards.elementAt(position).section
//        holder.tvValidityDateValue.text = cards.elementAt(position).validityDate.toString()
    }
}