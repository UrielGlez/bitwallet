package mx.uach.bitwallet

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import org.json.JSONObject
import mx.uach.bitwallet.interfaces.ServerCallback




// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment(){
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var preferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private var sesion: String = "sesion"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        preferences = activity?.getSharedPreferences("sesiones", Context.MODE_PRIVATE)!!
        editor = preferences.edit()

        if(checkSesion()) {
            activity?.finish()
            startActivity(Intent(context, HomeActivity::class.java))
        }

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
            preferences = activity?.getPreferences(Context.MODE_PRIVATE)!!
            editor = preferences.edit()
        }

    }

    private fun checkSesion(): Boolean {
        return this.preferences.getBoolean(sesion, false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val btnLogin = view.findViewById<Button>(R.id.btn_Login)

        val btnRegister = view.findViewById<TextView>(R.id.btn_register_link)
        val btnForgotPassword = view.findViewById<TextView>(R.id.btn_forgot_password_link)

        btnForgotPassword.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.recoverPasswordFragment)
        })

        btnRegister.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.registerFragment)
        })

        btnLogin.setOnClickListener(View.OnClickListener {
            //startActivity(Intent(context, HomeActivity::class.java))
            //Navigation.findNavController(view).navigate(R.id.homeFragment)
            val email = view.findViewById<TextInputEditText>(R.id.email_text_input).text
            val pass = view.findViewById<TextInputEditText>(R.id.pass_text_input).text
            DataService.instance.onLogin(context, email.toString(), pass.toString(), object : ServerCallback {
                override fun onSuccess(response: JSONObject?, statusCode: Int) {
                    if(statusCode == 200) {
                        val result: java.util.HashMap<*, *> = Gson().fromJson(response.toString(), HashMap::class.java)
                        saveLogin(view, result)
                        activity?.finish()
                        startActivity(Intent(context, HomeActivity::class.java))
                    } else {
                        Toast.makeText(context, "Error al iniciar sesion",Toast.LENGTH_SHORT).show()
                        //Log.i("Error", "Alex has un toast")
                    }

                }
            })
        })

    }

    private fun saveLogin(view: View, result: java.util.HashMap<*, *>) {
        editor.putBoolean(sesion, true)
        editor.putString("token", result["token"].toString())
        editor.putString("user", result["user"].toString())
        editor.apply()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LoginFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LoginFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}