package mx.uach.bitwallet

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pranavpandey.android.dynamic.toasts.DynamicToast
import mx.uach.bitwallet.adapters.CardAdapter
import mx.uach.bitwallet.interfaces.ServerStringCallback
import mx.uach.bitwallet.models.Card
import mx.uach.bitwallet.models.TypeCard
import org.json.JSONObject
import java.time.LocalDateTime


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var typeCard: String? = null
    private val model: SharedViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        typeCard = activity?.intent!!.getStringExtra("type_card").toString()

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var recyclerView = view.findViewById<RecyclerView>(R.id.homeItemsRecycler)
        var filterTitle = view.findViewById<TextView>(R.id.menu_filter_title)
        var date = view.findViewById<TextView>(R.id.date_text)

        filterTitle.text = getTitle()
        date.text = getDateFormat()

        val preferences: SharedPreferences = activity?.getSharedPreferences(
            "sesiones",
            Context.MODE_PRIVATE
        )!!

        var dividerItemDecoration: DividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            DividerItemDecoration.VERTICAL
        );
        //dividerItemDecoration.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.item_divider)!!)
        recyclerView.addItemDecoration(dividerItemDecoration);

        val user: HashMap<*, *> = Gson().fromJson(
            preferences.getString("user", null),
            HashMap::class.java
        )
        typeCard?.let {
            DataService.instance.getAllUserDocuments(
                view,
                context,
                user["_id"].toString(),
                typeCard,
                object : ServerStringCallback {
                    override fun onSuccess(result: String, statusCode: Int) {
                        if (JSONObject(result).get("message") == "Tarjetas cargadas correctamente") {
                            val itemType = object : TypeToken<List<Card>>() {}.type
                            var cards: List<Card> = Gson().fromJson(
                                JSONObject(result).getJSONObject("objs").get(
                                    "docs"
                                ).toString(), itemType
                            )
                            cards = cards.sorted()
                            var emptyView = view.findViewById<TextView>(R.id.noItemsText)
                            var anotherEmptyView =
                                view.findViewById<TextView>(R.id.addDocumentSuggestText)
                            var recyclerVieww =
                                view.findViewById<RecyclerView>(R.id.homeItemsRecycler)

                            if (cards.isEmpty()) {
                                recyclerVieww.visibility = View.GONE
                                emptyView.visibility = View.VISIBLE
                                anotherEmptyView.visibility = View.VISIBLE
                            } else {
                                recyclerVieww.visibility = View.VISIBLE
                                emptyView.visibility = View.GONE
                                anotherEmptyView.visibility = View.GONE
                            }
                            val adapter = CardAdapter(cards)
                            Log.i(
                                "dqwd",
                                JSONObject(result).getJSONObject("objs").get("docs").toString()
                            )

                            val rvCard = view.findViewById<RecyclerView>(R.id.homeItemsRecycler)
                            rvCard.adapter = adapter
                            rvCard.layoutManager = LinearLayoutManager(view.context)
                            rvCard.addOnItemTouchListener(
                                RecyclerItemClickListener(
                                    context,
                                    rvCard,
                                    object : RecyclerItemClickListener.OnItemClickListener {
                                        override fun onItemClick(view: View?, position: Int) {
                                            if (view != null) {
                                                model.select(cards[position])
                                                model.setCreateMode(false)

                                                when (cards[position]._type_card) {
                                                    TypeCard.DRIVER_LICENSE.toString() ->  Navigation.findNavController(
                                                        view
                                                    ).navigate(R.id.addingDLFragment)
                                                    TypeCard.CREDIT_DEBIT_CARD.toString() -> Navigation.findNavController(
                                                        view
                                                    ).navigate(R.id.addingDCCardFragment)
                                                    TypeCard.GIFT_CARD.toString() -> Navigation.findNavController(
                                                        view
                                                    ).navigate(R.id.addingGCFragment)
                                                    TypeCard.ID_CARD.toString() -> Navigation.findNavController(
                                                        view
                                                    ).navigate(R.id.addingIDFragment)
                                                    TypeCard.LICENSE_PLATE.toString() -> Navigation.findNavController(
                                                        view
                                                    ).navigate(R.id.addingCarPlateFragment)
                                                }
                                            }
                                        }

                                        override fun onLongItemClick(view: View?, position: Int) {
                                            context?.let { it1 ->
                                                AlertDialog.Builder(it1)
                                                    .setTitle("Borrar documento")
                                                    .setMessage("Estas seguro de borrar este documento de tu cartera?") // Specifying a listener allows you to take an action before dismissing the dialog.
                                                    // The dialog is automatically dismissed when a dialog button is clicked.
                                                    .setPositiveButton(
                                                        android.R.string.ok
                                                    ) { dialog, which ->
                                                        DataService.instance.deleteItem(
                                                            context,
                                                            cards[position],
                                                            object : ServerStringCallback {
                                                                override fun onSuccess(
                                                                    result: String,
                                                                    statusCode: Int
                                                                ) {
                                                                    if (JSONObject(result).get("message") == "Tarjeta eliminada correctamente") {
                                                                        activity!!.recreate()
                                                                        DynamicToast.makeSuccess(context!!, "Documento borrado", Toast.LENGTH_SHORT).show()
                                                                    } else {
                                                                        DynamicToast.makeError(context!!, "Error al borrar", Toast.LENGTH_SHORT).show()
                                                                    }
                                                                }

                                                            })
                                                    } // A null listener allows the button to dismiss the dialog and take no further action.
                                                    .setNegativeButton(android.R.string.cancel) { dialog, which ->
                                                        // Continue with delete operation
                                                    } // A null listener allows the button to dismiss the dialog and take no further action.)
                                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                                    .show()
                                            }
                                        }

                                    })
                            )
                        } else {
                            DynamicToast.makeError(
                                context!!,
                                "Error al cargar los documentos",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }
                })
        }

        val btnAddDocument = view.findViewById<FloatingActionButton>(R.id.addFloatingButton)

        btnAddDocument.setOnClickListener(View.OnClickListener {
            model.setCreateMode(true)
            Navigation.findNavController(view).navigate(R.id.documentTypeFragment)
        })

    }

    private fun getDateFormat(): String {
        val current = LocalDateTime.now()
        var month: String = ""
        when(current.monthValue) {
            1 -> month = "enero"
            2 -> month = "febrero"
            3 -> month = "marzo"
            4 -> month = "abril"
            5 -> month = "mayo"
            6 -> month = "junio"
            7 -> month = "julio"
            8 -> month = "agosto"
            9 -> month = "septiembre"
            10 -> month = "octubre"
            11 -> month = "noviembre"
            12 -> month = "diciembre"
        }

        return "${current.dayOfMonth} de $month de ${current.year}"
    }

    private fun getTitle(): String {
        var title: String = ""
        when(typeCard) {
            "null" -> title = "Todos tus documentos"
            TypeCard.CREDIT_DEBIT_CARD.toString() -> title = "Tus tarjatas de Credito/Debito"
            TypeCard.GIFT_CARD.toString() -> title = "Tus tarjetas club"
            TypeCard.ID_CARD.toString() -> title = "Tus identificaciones"
            TypeCard.DRIVER_LICENSE.toString() -> title = "Tus licencias"
            TypeCard.LICENSE_PLATE.toString() -> title = "Tus matriculas"
        }

        return title
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}