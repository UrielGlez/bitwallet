package mx.uach.bitwallet

import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.pranavpandey.android.dynamic.toasts.DynamicToast
import mx.uach.bitwallet.interfaces.ServerCallback
import mx.uach.bitwallet.models.Card
import mx.uach.bitwallet.models.TypeCard
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [addingCarPlateFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class addingCarPlateFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private val model: SharedViewModel by activityViewModels()
    private var createMode: Boolean = true
    private var cardId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adding_car_plate, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var pass: String = ""
        val preferences: SharedPreferences = activity?.getSharedPreferences("sesiones", Context.MODE_PRIVATE)!!
        val user: HashMap<*, *> = Gson().fromJson(preferences.getString("user", null), HashMap::class.java)

        val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(context)
        builder.setTitle("Ingresar PIN")

        val viewInflated: View =
            LayoutInflater.from(context).inflate(R.layout.password_dialog, getView() as ViewGroup?, false)

        val input = viewInflated.findViewById<EditText>(R.id.password_text)

        input.setText(pass)

        builder.setView(viewInflated)

        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
            val aDouble: Double = user["_pin"] as Double
            if (input.text.toString() == aDouble.toInt().toString()) {
                dialog.dismiss()
            } else {
                dialog.cancel()
                Navigation.findNavController(
                    view
                ).popBackStack()
                DynamicToast.makeError(requireContext(), "PIN incorrecto", Toast.LENGTH_SHORT).show()
            }
        }

        builder.setNegativeButton(
            "Cancelar"
        ) { dialog, which ->
            run {
                dialog.cancel()
                Navigation.findNavController(
                    view
                ).popBackStack()
            }
        }

        builder.setCancelable(false)
        builder.show()

        val btnCarPlate = view.findViewById<TextView>(R.id.btn_cplateadd)
        val btnAddNote = view.findViewById<TextView>(R.id.add_note)
        val textNotes = view.findViewById<TextView>(R.id.notes)


        val titleText = view.findViewById<TextInputEditText>(R.id.plate_title)
        val carModelText = view.findViewById<TextInputEditText>(R.id.plate_model)
        val serieText = view.findViewById<TextInputEditText>(R.id.plate_serie)
        val stateText = view.findViewById<TextInputEditText>(R.id.plate_state)
        val notesText = view.findViewById<TextView>(R.id.notes)

        model.createMode.observe(viewLifecycleOwner, Observer<Boolean> {
            createMode = it
            if(it) {
                btnCarPlate.text = "Agregar"
            }else {
                btnCarPlate.text = "Editar"
                model.selected.observe(viewLifecycleOwner, Observer<Card> {
                    titleText.setText(it._title)
                    carModelText.setText(it._car_model)
                    serieText.setText(it._serie)
                    stateText.setText(it._state)
                    notesText.text = it._notes
                })
            }
        })

        btnCarPlate.setOnClickListener(View.OnClickListener {

            var card = Card(
                _type_card = TypeCard.LICENSE_PLATE.toString(),
                _notes = notesText.text.toString(),
                _title = titleText.text.toString(),
                _car_model = carModelText.text.toString(),
                _serie = serieText.text.toString(),
                _state = stateText.text.toString(),
                _card_number = "",
                _validity_date = Date(),
                _account_name = "",
                _curp = "",
                _municipality = "",
                _section = "",
                _user_id = ""
            )

            if(createMode) {
                DataService.instance.createItem(context, card, object : ServerCallback {
                    override fun onSuccess(response: JSONObject?, statusCode: Int) {
                        if (statusCode == 200) {
                            Navigation.findNavController(view).navigate(R.id.homeFragment)
                        } else {
                            Toast.makeText(context, "Error al crear", Toast.LENGTH_SHORT).show()
                            //Log.i("Error", "Alex has un toast")
                        }

                    }
                }, user["_id"].toString())
            }else {
                DataService.instance.editItem(context, card, object : ServerCallback {
                    override fun onSuccess(response: JSONObject?, statusCode: Int) {
                        if (statusCode == 200) {
                            Navigation.findNavController(view).navigate(R.id.homeFragment)
                        } else {
                            Toast.makeText(context, "Error al editar", Toast.LENGTH_SHORT).show()
                            //Log.i("Error", "Alex has un toast")
                        }

                    }
                }, cardId)
            }
        })

        btnAddNote.setOnClickListener(View.OnClickListener {
            // -----------------
            val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(context)
            builder.setTitle("Agregar Nota")

            val viewInflated: View =
                LayoutInflater.from(context).inflate(R.layout.custom_dialog, getView() as ViewGroup?, false)

            val input = viewInflated.findViewById<View>(R.id.input_note) as EditText

            input.setText(textNotes.text.toString())

            builder.setView(viewInflated)

            builder.setPositiveButton(android.R.string.ok, DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
                textNotes.text = input.text.toString()
            })

            builder.setNegativeButton(
                android.R.string.cancel,
                DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })

            builder.show()

            // -----------------
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment addingCarPlateFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            addingCarPlateFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}