package mx.uach.bitwallet

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import mx.uach.bitwallet.interfaces.ServerCallback
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val btnRegister = view.findViewById<Button>(R.id.btn_register)

        btnRegister.setOnClickListener(View.OnClickListener {
            val email = view.findViewById<TextInputEditText>(R.id.email_text_input).text?.trim()
            val pass = view.findViewById<TextInputEditText>(R.id.pass_text_input).text?.trim()
            val firstName = view.findViewById<TextInputEditText>(R.id.first_name_text_input).text
            val lastName = view.findViewById<TextInputEditText>(R.id.last_name_text_input).text
            val confirmPass = view.findViewById<TextInputEditText>(R.id.confirm_pass_text_input).text?.trim()
            val pin = view.findViewById<TextInputEditText>(R.id.pin_text_input).text?.trim()

            if(confirmPass == pass) {
                DataService.instance.onRegister(context, email.toString(), pass.toString(),
                    firstName.toString(), lastName.toString(), pin.toString(), object : ServerCallback {
                        override fun onSuccess(response: JSONObject?, statusCode: Int) {
                            if(statusCode == 200) {
                                Navigation.findNavController(view).navigate(R.id.loginFragment)
                            }else {
                                Toast.makeText(context, "Error al registrarse", Toast.LENGTH_SHORT).show()
                                //Log.i("Error", "Alex has un toast")
                            }
                        }
                    })
            }else {
                Toast.makeText(context, "Error al registrarse", Toast.LENGTH_SHORT).show()
                //Log.i("Error", "Alex has un toasttttttt")
            }

        })

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RegisterFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RegisterFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}