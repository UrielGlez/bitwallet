package mx.uach.bitwallet

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.NetworkResponse
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.pranavpandey.android.dynamic.toasts.DynamicToast
import mx.uach.bitwallet.interfaces.ServerCallback
import mx.uach.bitwallet.interfaces.ServerStringCallback
import mx.uach.bitwallet.models.Card
import org.json.JSONObject


class DataService {
    private val url = "https://b695b08c4ccb.ngrok.io"
    private lateinit var queue: RequestQueue
    private var statusCode: Int = 0

    companion object {
        val instance = DataService()
    }

    fun onLogin(context: Context?, email: String, pass: String, callback: ServerCallback): Unit {
        queue = Volley.newRequestQueue(context)

        var jsonBody: JSONObject = JSONObject()
        jsonBody.put("email", email)
        jsonBody.put("password", pass)
        val jsonObjectRequest =  object: JsonObjectRequest(
            Method.POST, "$url/signin", jsonBody,
            { response ->
                callback.onSuccess(response, statusCode)
            },
            { error ->
                    DynamicToast.makeError(context!!, "Error al iniciar sesion", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getHeaders(): MutableMap<String, String> {
                val token: String = "test"
                val headers = HashMap<String, String>()
                //headers["Authorization"] = "Bearer $token"
                return headers
            }
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject>? {
                if (response != null) {
                    statusCode = response.statusCode
                }
                return super.parseNetworkResponse(response)
            }
        }
        queue.add(jsonObjectRequest)
    }

    fun onRegister(context: Context?, email: String, password: String, firstName: String,
                lastName: String, pin: String, callback: ServerCallback): Unit {
        queue = Volley.newRequestQueue(context)

        var jsonBody: JSONObject = JSONObject()
        jsonBody.put("email", email)
        jsonBody.put("password", password)
        jsonBody.put("firstName", firstName)
        jsonBody.put("lastName", lastName)
        jsonBody.put("pin", pin)

        val jsonObjectRequest =  object: JsonObjectRequest(
            Method.POST, "$url/signup", jsonBody,
            { response ->
                callback.onSuccess(response, statusCode)
            },
            { error ->
                    DynamicToast.makeError(context!!, "Error al registrarse", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getHeaders(): MutableMap<String, String> {
                val token: String = "test"
                val headers = HashMap<String, String>()
                //headers["Authorization"] = "Bearer $token"
                return headers
            }
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject>? {
                if (response != null) {
                    statusCode = response.statusCode
                }
                return super.parseNetworkResponse(response)
            }
        }
        queue.add(jsonObjectRequest)
    }

    fun getAllUserDocuments(view: View, context: Context?, userId: String, typeCard: String?, callback: ServerStringCallback) {
        queue = Volley.newRequestQueue(context)

        val customUrl: String = if(typeCard == "null")
            "$url/cards/$userId"
        else
            "$url/cards/$userId/$typeCard"

        Log.i("URL", "$customUrl")
        val stringRequest = object: StringRequest(
            Request.Method.GET, customUrl,
            Response.Listener { response ->
                callback.onSuccess(response, statusCode)
            },
            Response.ErrorListener{ error ->
                DynamicToast.makeError(context!!, "Error al cargar los documentos", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getHeaders(): MutableMap<String, String> {
                val token: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwYTA1NjFjMjUwYzM0MzE2ODRjYjYwZCIsImlhdCI6MTYyMTU0NjgyMX0.nPpnqtpj3gizGbv2uEpYwh5_v4icCnSlBCnEd04dRvw"
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $token"
                return headers
            }
        }

        queue.add(stringRequest)
    }

    fun createItem(context: Context?, card: Card, callback: ServerCallback, clientId: String) {
        queue = Volley.newRequestQueue(context)
        Log.i("HERE", "${card._validity_date}")

        var jsonBody: JSONObject = JSONObject()
        jsonBody.put("typeCard", card._type_card)
        jsonBody.put("title", card._title)
        jsonBody.put("notes", card._notes)
        jsonBody.put("cardNumber", card._card_number)
        jsonBody.put("validityDate", card._validity_date)
        jsonBody.put("curp", card._curp)
        jsonBody.put("state", card._state)
        jsonBody.put("serie", card._serie)
        jsonBody.put("section", card._section)
        jsonBody.put("carModel", card._car_model)
        jsonBody.put("accountName", card._account_name)
        jsonBody.put("municipality", card._municipality)
        jsonBody.put("userID", clientId)

        val jsonObjectRequest =  object: JsonObjectRequest(
            Method.POST, "$url/cards/", jsonBody,
            { response ->
                callback.onSuccess(response, statusCode)
            },
            { error ->
                    DynamicToast.makeError(context!!, "Error al crear los documentos", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getHeaders(): MutableMap<String, String> {
                val token: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwYTA1NjFjMjUwYzM0MzE2ODRjYjYwZCIsImlhdCI6MTYyMTU0NjgyMX0.nPpnqtpj3gizGbv2uEpYwh5_v4icCnSlBCnEd04dRvw"
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $token"
                return headers
            }
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject>? {
                if (response != null) {
                    statusCode = response.statusCode
                }
                return super.parseNetworkResponse(response)
            }
        }
        queue.add(jsonObjectRequest)

    }

    fun deleteItem(context: Context?, card: Card, callback: ServerStringCallback) {
        queue = Volley.newRequestQueue(context)

        val stringRequest = object: StringRequest(
            Request.Method.DELETE, "$url/cards/${card._id}",
            Response.Listener { response ->
                callback.onSuccess(response, statusCode)
            },
            Response.ErrorListener{ error ->
                DynamicToast.makeError(context!!, "Error al borrar el documento", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getHeaders(): MutableMap<String, String> {
                val token: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwYTA1NjFjMjUwYzM0MzE2ODRjYjYwZCIsImlhdCI6MTYyMTU0NjgyMX0.nPpnqtpj3gizGbv2uEpYwh5_v4icCnSlBCnEd04dRvw"
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $token"
                return headers
            }
        }

        queue.add(stringRequest)
    }

    fun editItem(context: Context?, card: Card, callback: ServerCallback, cardId: String) {
        queue = Volley.newRequestQueue(context)

        var jsonBody: JSONObject = JSONObject()
        jsonBody.put("title", card._title)
        jsonBody.put("notes", card._notes)
        jsonBody.put("cardNumber", card._card_number)
        jsonBody.put("validity", card._validity_date)
        jsonBody.put("curp", card._curp)
        jsonBody.put("state", card._state)
        jsonBody.put("serie", card._serie)
        jsonBody.put("section", card._section)
        jsonBody.put("carModel", card._car_model)
        jsonBody.put("accountName", card._account_name)
        jsonBody.put("municipality", card._municipality)

        val jsonObjectRequest =  object: JsonObjectRequest(
            Method.PUT, "$url/cards/$cardId", jsonBody,
            { response ->
                callback.onSuccess(response, statusCode)
            },
            { error ->
                DynamicToast.makeError(context!!, "Error al editar los documentos", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getHeaders(): MutableMap<String, String> {
                val token: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwYTA1NjFjMjUwYzM0MzE2ODRjYjYwZCIsImlhdCI6MTYyMTU0NjgyMX0.nPpnqtpj3gizGbv2uEpYwh5_v4icCnSlBCnEd04dRvw"
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $token"
                return headers
            }
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject>? {
                if (response != null) {
                    statusCode = response.statusCode
                }
                return super.parseNetworkResponse(response)
            }
        }
        queue.add(jsonObjectRequest)
    }
}